package be.zoekerscript.zoekertjes;

/**
 * Created by Caroline on 28-9-2016.
 */
public enum ZoekerTypes {

    DEFAULT_STATE(new DefaultState()),
    PROMO_STATE(new PromoState()),
    SPOTLIGHT_STATE(new SpotlightState());

    private final ZoekerTypeState state;

    public ZoekerTypeState getState() {
        return state;
    }
    private ZoekerTypes(ZoekerTypeState zoekerTypeState) {
        this.state = zoekerTypeState;
    }

}

package be.zoekerscript.zoekertjes;

/**
 * Created by Caroline on 17-9-2016.
 */
public class ZoekerStateException extends Exception {

    public ZoekerStateException(String msg) {
        super(msg);
    }

}

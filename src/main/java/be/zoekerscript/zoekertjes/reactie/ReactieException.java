package be.zoekerscript.zoekertjes.reactie;

/**
 * Created by Caroline on 28-9-2016.
 */
public class ReactieException extends Exception {
    public ReactieException (String message) { super(message); }
}

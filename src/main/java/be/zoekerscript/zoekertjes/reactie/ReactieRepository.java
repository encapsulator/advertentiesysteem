package be.zoekerscript.zoekertjes.reactie;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Niels on 10/1/2016.
 */
public interface ReactieRepository extends JpaRepository<Reactie, Integer> {
}

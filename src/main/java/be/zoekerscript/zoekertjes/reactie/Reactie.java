package be.zoekerscript.zoekertjes.reactie;

import be.zoekerscript.zoekertjes.Zoekertje;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Caroline on 28-9-2016.
 */
@Entity
@Table
public class Reactie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String boodschap;
    @Column
    private Date datum;
    @Column
    private int gebruikersId;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.LAZY)
    @JoinColumn(name = "zoekertje_id")
    private Zoekertje zoekertje;

    public Reactie() {
    }

    public Reactie(String boodschap, Date datum, int gebruikersId) {
        this.boodschap = boodschap;
        this.datum = datum;
        this.gebruikersId = gebruikersId;
    }

    public Reactie(String boodschap, Date datum, int gebruikersId, Zoekertje zoekertje) {
        this(boodschap, datum, gebruikersId);
        this.zoekertje = zoekertje;
    }

    public String getBoodschap() {
        return boodschap;
    }

    public void setBoodschap(String boodschap) {
        this.boodschap = boodschap;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public int getGebruikersId() {
        return gebruikersId;
    }

    public void setGebruikersId(int gebruikersId) {
        this.gebruikersId = gebruikersId;
    }
}

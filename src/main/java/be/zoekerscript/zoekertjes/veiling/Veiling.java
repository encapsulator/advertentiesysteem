package be.zoekerscript.zoekertjes.veiling;

import be.zoekerscript.zoekertjes.Zoekertje;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;


/**
 * Created by Caroline on 24-9-2016.
 */
@Entity
@Table
public class Veiling implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private int minimumBod;
    @Column
    private int koopNu;
    @Column
    private int opbiedenMet;

    @OneToMany(cascade ={CascadeType.ALL}, fetch = FetchType.EAGER, mappedBy = "veiling", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Bod> boden;

    public Veiling() {
    }

    public Veiling(int minimumBod, int koopNu, int opbiedenMet) {
        this.minimumBod = minimumBod;
        this.koopNu = koopNu;
        this.opbiedenMet = opbiedenMet;
        this.boden = new ArrayList<>();
    }

    public int getMinimumBod() {
        return minimumBod;
    }

    public void setMinimumBod(int minimumBod) {
        this.minimumBod = minimumBod;
    }

    public int getKoopNu() {
        return koopNu;
    }

    public void setKoopNu(int koopNu) {
        this.koopNu = koopNu;
    }

    public int getOpbiedenMet() {
        return opbiedenMet;
    }

    public void setOpbiedenMet(int opbiedenMet) {
        this.opbiedenMet = opbiedenMet;
    }

    public Collection<Bod> getBoden() {
        return boden;
    }

    public void setBoden(Collection<Bod> boden) {
        this.boden = boden;
    }

    public Bod getHoogsteBod() {
        if(boden.size() == 0) return null;
        if(boden.size() == 1) return boden.stream().collect(Collectors.toList()).get(0);
        return boden.stream().max(new Comparator<Bod>() {

            @Override
            public int compare(Bod o1, Bod o2) {

                return o1.getWaarde()-o2.getWaarde();
            }
        }).get();
    }

    private int getHoogsteBodWaarde() {
        Bod hoogsteBod = getHoogsteBod();
        if(hoogsteBod == null) return -1;
        return hoogsteBod.getWaarde();
    }

    public void addBod(Bod bod) throws BodInvalidException {
        int bedrag = bod.getWaarde();
        if(bedrag < minimumBod) throw new BodInvalidException("BOD_UNDER_MINIMUM");
        if(bedrag <= getHoogsteBodWaarde()) throw new BodInvalidException("ALREADY_HIGHER_BOD");
        if(bedrag > koopNu) throw new BodInvalidException("BOD_OVER_BUY_NOW");
        boden.add(bod);
    }
}

package be.zoekerscript.zoekertjes.veiling;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Caroline on 24-9-2016.
 */
@Entity
@Table
public class Bod {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private int gebruikersId;
    @Column
    private int waarde;
    @Column
    private Date datum;

    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "veiling_id")
    private Veiling veiling;

    public Bod() {
    }

    public Bod(int waarde, Date datum) {
        this.waarde = waarde;
        this.datum = datum;
    }

    public Bod(int gebruikersId, int waarde, Date datum) {
        this(waarde, datum);
        this.gebruikersId = gebruikersId;
    }

    public Bod(int gebruikersId, int waarde, Date datum, Veiling veiling) {
        this(gebruikersId, waarde, datum);
        this.veiling = veiling;
    }

    public int getGebruikersId() {
        return gebruikersId;
    }

    public void setGebruikersId(int gebruikersId) {
        this.gebruikersId = gebruikersId;
    }

    public int getWaarde() {
        return waarde;
    }

    public void setWaarde(int waarde) {
        this.waarde = waarde;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
}

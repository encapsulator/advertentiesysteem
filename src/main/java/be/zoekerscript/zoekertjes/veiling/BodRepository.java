package be.zoekerscript.zoekertjes.veiling;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Niels on 10/1/2016.
 */
public interface BodRepository extends JpaRepository<Bod, Integer> {
}

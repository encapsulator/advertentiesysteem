package be.zoekerscript.zoekertjes.veiling;

/**
 * Created by Caroline on 24-9-2016.
 */
public class BodInvalidException extends Exception {

    public BodInvalidException(String message) {
        super(message);
    }

}

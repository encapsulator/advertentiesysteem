package be.zoekerscript.zoekertjes;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Caroline on 30-9-2016.
 */
public interface ZoekerRepository extends JpaRepository<Zoekertje, Integer> {
}

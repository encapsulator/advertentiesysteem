package be.zoekerscript.zoekertjes;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.stream.Collectors;

import static be.zoekerscript.zoekertjes.ZoekerTypes.*;

/**
 * Created by Caroline on 17-9-2016.
 */
public abstract class ZoekerTypeState {

    private Enum ZoekerTypes;

    public void changeState(Zoekertje zoekertje, ZoekerTypeState zoekerTypeState) throws ZoekerStateException {
        String type = Arrays.stream(values()).filter(x -> x.getState().equals(zoekerTypeState)).collect(Collectors.toList()).get(0).name();
        if(type.equals("DEFAULT_STATE")) this.setDefaultState(zoekertje);
        if(type.equals("PROMO_STATE")) this.setPromoState(zoekertje);
        if(type.equals("SPOTLIGHT_STATE")) this.setSpotlightState(zoekertje);
    }

    protected void setPromoState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new NotImplementedException();
    }
    protected void setSpotlightState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new NotImplementedException();
    }
    protected void setDefaultState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new NotImplementedException();
    }



}

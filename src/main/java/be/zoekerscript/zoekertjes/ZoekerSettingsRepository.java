package be.zoekerscript.zoekertjes;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Niels on 10/1/2016.
 */
public interface ZoekerSettingsRepository extends JpaRepository<ZoekerSettings, Integer> {
}

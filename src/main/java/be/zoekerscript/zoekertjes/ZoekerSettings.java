package be.zoekerscript.zoekertjes;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Caroline on 28-9-2016.
 */
@Entity
@Table(name = "zoekertje_settings")
public class ZoekerSettings {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private Date inPromoSinds;
    @Column
    private Date inSpotlightSinds;
    @Column
    private boolean reactiesToegestaan;
    @Column
    private boolean isVeiling;

    public ZoekerSettings() {
    }

    public ZoekerSettings(Date inPromoSinds, Date inSpotlightSinds, boolean reactiesToegestaan, boolean isVeiling) {
        this.inPromoSinds = inPromoSinds;
        this.inSpotlightSinds = inSpotlightSinds;
        this.reactiesToegestaan = reactiesToegestaan;
        this.isVeiling = isVeiling;
    }

    public Date getInPromoSinds() {
        return inPromoSinds;
    }

    public void setInPromoSinds(Date inPromoSinds) {
        this.inPromoSinds = inPromoSinds;
    }

    public Date getInSpotlightSinds() {
        return inSpotlightSinds;
    }

    public void setInSpotlightSinds(Date inSpotlightSinds) {
        this.inSpotlightSinds = inSpotlightSinds;
    }

    public boolean isReactiesToegestaan() {
        return reactiesToegestaan;
    }

    public void setReactiesToegestaan(boolean reactiesToegestaan) {
        this.reactiesToegestaan = reactiesToegestaan;
    }

    public boolean isIsVeiling() {
        return isVeiling;
    }

    public void setIsVeiling(boolean veiling) {
        this.isVeiling = veiling;
    }
}

package be.zoekerscript.zoekertjes;

/**
 * Created by Caroline on 17-9-2016.
 */
public class SpotlightState extends ZoekerTypeState {
    @Override
    protected void setPromoState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new ZoekerStateException("ALREADY_PROMO");
    }
    @Override
    protected void setSpotlightState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new ZoekerStateException("SAME_STATE");
    }
    @Override
    protected void setDefaultState(Zoekertje zoekertje) throws ZoekerStateException {
        zoekertje.setZoekerTypeState(new DefaultState());
    }

    @Override
    public boolean equals(Object o) {
        if(o != null && o instanceof SpotlightState) {
            return true;
        }
        return false;
    }
}

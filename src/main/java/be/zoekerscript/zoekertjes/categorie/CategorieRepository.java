package be.zoekerscript.zoekertjes.categorie;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Niels on 10/1/2016.
 */
public interface CategorieRepository extends JpaRepository<Categorie, Integer> {
}

package be.zoekerscript.zoekertjes.categorie;

import be.zoekerscript.zoekertjes.Zoekertje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Caroline on 17-9-2016.
 */
@Entity
@Table
public class Categorie {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private String naam;

    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, mappedBy = "categorie")
    private Collection<Zoekertje> zoekertjes;

    public Categorie() {
        zoekertjes = new ArrayList<>();
    }

    public Categorie(String naam) {
        this.naam = naam;
    }

    public String getNaam() {
        return naam;
    }

    public void setNaam(String naam) {
        this.naam = naam;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Collection<Zoekertje> getZoekertjes() {
        return zoekertjes;
    }

    public void setZoekertjes(Collection<Zoekertje> zoekertjes) {
        this.zoekertjes = zoekertjes;
    }

    public void addZoekertje(Zoekertje zoekertje) {
        zoekertjes.add(zoekertje);
    }
}

package be.zoekerscript.zoekertjes;

import be.zoekerscript.zoekertjes.reactie.Reactie;
import be.zoekerscript.zoekertjes.reactie.ReactieException;
import be.zoekerscript.zoekertjes.categorie.Categorie;
import be.zoekerscript.zoekertjes.veiling.Bod;
import be.zoekerscript.zoekertjes.veiling.BodInvalidException;
import be.zoekerscript.zoekertjes.veiling.Veiling;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.*;
import java.util.stream.Collectors;

import static be.zoekerscript.zoekertjes.ZoekerTypes.values;

/**
 * Created by Caroline on 17-9-2016.
 */
@Entity
@Table
public class Zoekertje {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column
    private int gebruikersId;
    @Column
    private String titel;
    @Column
    private String tekst;
    @Column
    private Date datumToegevoegd;
    @Column
    private Date datumGewijzigd;
    @Column
    private int aantalBekeken;
    @Transient
    private ZoekerTypeState zoekerTypeState;
    @Column
    private String state;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.DETACH})
    @JoinColumn(name = "categorieId")
    private Categorie categorie;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private Veiling veiling;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "zoekertje", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private Collection<Reactie> reacties;

    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ZoekerSettings settings;

    public Zoekertje() {
    }

    public Zoekertje(int gebruikersId, String titel, String tekst, Date datumToegevoegd, Date datumGewijzigd, int aantalBekeken, ZoekerTypeState zoekerTypeState, Categorie categorie, Veiling veiling, Collection<Reactie> reacties, ZoekerSettings settings) {
        this.gebruikersId = gebruikersId;
        this.titel = titel;
        this.tekst = tekst;
        this.datumToegevoegd = datumToegevoegd;
        this.datumGewijzigd = datumGewijzigd;
        this.aantalBekeken = aantalBekeken;
        this.zoekerTypeState = zoekerTypeState;
        this.state = Arrays.stream(values()).filter(x -> x.getState().equals(zoekerTypeState)).collect(Collectors.toList()).get(0).name();
        this.categorie = categorie;
        this.veiling = veiling;
        this.reacties = reacties;
        this.settings = settings;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getTekst() {
        return tekst;
    }

    public void setTekst(String tekst) {
        this.tekst = tekst;
    }

    public Date getDatumToegevoegd() {
        return datumToegevoegd;
    }

    public void setDatumToegevoegd(Date datumToegevoegd) {
        this.datumToegevoegd = datumToegevoegd;
    }

    public Date getDatumGewijzigd() {
        return datumGewijzigd;
    }

    public void setDatumGewijzigd(Date datumGewijzigd) {
        this.datumGewijzigd = datumGewijzigd;
    }

    public int getAantalBekeken() {
        return aantalBekeken;
    }

    public void setAantalBekeken(int aantal_bekeken) {
        this.aantalBekeken = aantal_bekeken;
    }

    public ZoekerTypeState getZoekerTypeState() {
        return zoekerTypeState;
    }

    public void setZoekerTypeState(ZoekerTypeState zoekerTypeState) {
        this.zoekerTypeState = zoekerTypeState;
    }

    public void changeZoekerTypeState(ZoekerTypeState zoekerTypeState) throws ZoekerStateException {
        this.zoekerTypeState.changeState(this, zoekerTypeState);
    }

    public Categorie getCategorie() {
        return categorie;
    }

    public void setCategorie(Categorie categorie) {
        this.categorie = categorie;
    }

    public int getGebruikersId() {
        return gebruikersId;
    }

    public void setGebruikersId(int gebruikersId) {
        this.gebruikersId = gebruikersId;
    }

    public Veiling getVeiling() {
        return veiling;
    }

    public void setVeiling(Veiling veiling) {
        this.veiling = veiling;
    }

    public void addBod(Bod b) throws BodInvalidException {
        if(gebruikersId == b.getGebruikersId()) throw new BodInvalidException("USER_OWNS_ZOEKERTJE");
        veiling.addBod(b);
    }

    private Reactie getLaatsteReactie() {
        if(reacties.size()==0)return null;
        return getReacties().stream().collect(Collectors.toList()).get(reacties.size()-1);
    }

    public void addReactie(Reactie reactie) throws ReactieException {
        if(!settings.isReactiesToegestaan()) throw new ReactieException("NOT_ALLOWED");
        Reactie laatsteReactie = getLaatsteReactie();
        if(laatsteReactie != null && laatsteReactie.getGebruikersId()==reactie.getGebruikersId()) {
            throw new ReactieException("SAME_USER_LAST_REACTION");
        } else if (laatsteReactie == null && gebruikersId == reactie.getGebruikersId()) {
            throw new ReactieException("OWNER_CANT_PLACE_FIRST_REACTION");
        }
        reacties.add(reactie);
    }

    public Collection<Reactie> getReacties() {
        return reacties;
    }

    public void setReacties(Collection<Reactie> reacties) {
        this.reacties = reacties;
    }

    public ZoekerSettings getSettings() {
        return settings;
    }

    public void setSettings(ZoekerSettings settings) {
        this.settings = settings;
    }

    public static class ZoekertjesBuilder {
        private int gebruikersId;
        private Veiling veiling;
        private String titel;
        private String tekst;
        private Date datumToegevoegd;
        private Date datumGewijzigd;
        private int aantal_bekeken;
        private ZoekerTypeState zoekerTypeState;
        private Categorie categorie;
        private Collection<Reactie> reacties;
        private ZoekerSettings settings;


        public ZoekertjesBuilder() {
            reacties = new ArrayList<>();
            settings = new ZoekerSettings(null, null, true, true);
        }

        public ZoekertjesBuilder setTitel(String titel) {
            this.titel = titel;
            return this;
        }

        public ZoekertjesBuilder setTekst(String tekst) {
            this.tekst = tekst;
            return this;
        }

        public ZoekertjesBuilder setDatumToegevoegd(Date datumToegevoegd) {
            this.datumToegevoegd = datumToegevoegd;
            return this;
        }

        public ZoekertjesBuilder setReacties(Collection<Reactie> reacties) {
            this.reacties = reacties;
            return this;
        }

        public ZoekertjesBuilder setDatumGewijzigd(Date datumGewijzigd) {
            this.datumGewijzigd = datumGewijzigd;
            return this;
        }

        public ZoekertjesBuilder setAantal_bekeken(int aantal_bekeken) {
            this.aantal_bekeken = aantal_bekeken;
            return this;
        }

        public ZoekertjesBuilder setZoekerTypeState(ZoekerTypeState zoekerTypeState) {
            this.zoekerTypeState = zoekerTypeState;
            return this;
        }

        public ZoekertjesBuilder setCategorie(Categorie categorie) {
            this.categorie = categorie;
            return this;
        }

        public ZoekertjesBuilder setGebruikersId(int gebruikersId) {
            this.gebruikersId = gebruikersId;
            return this;
        }

        public ZoekertjesBuilder setVeiling(Veiling veiling) {
            this.veiling = veiling;
            if(veiling.getMinimumBod() == veiling.getKoopNu()) {
                settings.setIsVeiling(false);
            }
            return this;
        }

        public ZoekertjesBuilder setSettings(ZoekerSettings settings) {
            this.settings = settings;
            return this;
        }

        public Zoekertje build() {
            return new Zoekertje(gebruikersId, titel, tekst, datumToegevoegd, datumGewijzigd, aantal_bekeken, zoekerTypeState, categorie, veiling, reacties, settings);
        }
    }
}

package be.zoekerscript.zoekertjes;

/**
 * Created by Caroline on 17-9-2016.
 */
public class DefaultState extends ZoekerTypeState {
    @Override
    protected void setPromoState(Zoekertje zoekertje) throws ZoekerStateException {
        zoekertje.setZoekerTypeState(new PromoState());
    }
    @Override
    protected void setSpotlightState(Zoekertje zoekertje) throws ZoekerStateException {
        zoekertje.setZoekerTypeState(new SpotlightState());
    }
    @Override
    protected void setDefaultState(Zoekertje zoekertje) throws ZoekerStateException {
        throw new ZoekerStateException("SAME_STATE");
    }

    @Override
    public boolean equals(Object o) {
        if(o != null && o instanceof DefaultState) {
            return true;
        }
        return false;
    }
}

package be.zoekerscript.gebruikers.berichten;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * Created by Caroline on 24-9-2016.
 */
@Entity
@Table(name = "bericht")
public class Bericht {

    @Id
    @GeneratedValue
    private Integer id;
    @Column
    private int gebruikerVan;
    @Column
    private int gebruikerNaar;
    @Column
    private String bericht;
    @Column
    private String titel;
    @Column
    private String berichtStatus;


    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "PARENT_ID")
    private Bericht parent;

    public Bericht() {
    }

    public Bericht(int gebruikerVan, int gebruikerNaar, String bericht, String titel, String berichtStatus, Bericht parent, Date datumVerstuurd) throws InvalidBerichtException {
        if(gebruikerVan == gebruikerNaar) throw new InvalidBerichtException("SAME_USER");
        this.gebruikerVan = gebruikerVan;
        this.gebruikerNaar = gebruikerNaar;
        this.bericht = bericht;

        if(!Arrays.stream(BerichtStatus.values()).map(x->x.name()).collect(Collectors.toList()).contains(berichtStatus)) {
            throw new InvalidBerichtException("INVALID_STATE");
        }

        this.berichtStatus = berichtStatus;
        this.parent = parent;
        if(parent != null) this.titel = "RE: " + parent.getTitel();
        else this.titel = titel;
        this.datumVerstuurd = datumVerstuurd;
    }

    private Date datumVerstuurd;

    public Bericht(Integer id, int gebruikerVan, int gebruikerNaar, String bericht, String titel, String berichtStatus, Bericht parent, Date datumVerstuurd) throws InvalidBerichtException {
        this(gebruikerVan, gebruikerNaar, bericht, titel, berichtStatus, parent, datumVerstuurd);
        this.id = id;
    }

    public int getGebruikerVan() {
        return gebruikerVan;
    }

    public void setGebruikerVan(int gebruikerVan) {
        this.gebruikerVan = gebruikerVan;
    }

    public int getGebruikerNaar() {
        return gebruikerNaar;
    }

    public void setGebruikerNaar(int gebruikerNaar) {
        this.gebruikerNaar = gebruikerNaar;
    }

    public String getBericht() {
        return bericht;
    }

    public void setBericht(String bericht) {
        this.bericht = bericht;
    }

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getBerichtStatus() {
        return berichtStatus;
    }

    public void setBerichtStatus(String berichtStatus) throws InvalidBerichtException {
        if(!Arrays.stream(BerichtStatus.values()).map(x->x.name()).collect(Collectors.toList()).contains(berichtStatus)) {
            throw new InvalidBerichtException("INVALID_STATE");
        }
        this.berichtStatus = berichtStatus;
    }

    public Bericht getParent() {
        return parent;
    }

    public void setParent(Bericht antwoordOp) {
        this.parent = antwoordOp;
    }

    public Date getDatumVerstuurd() {
        return datumVerstuurd;
    }

    public void setDatumVerstuurd(Date datumVerstuurd) {
        this.datumVerstuurd = datumVerstuurd;
    }
}

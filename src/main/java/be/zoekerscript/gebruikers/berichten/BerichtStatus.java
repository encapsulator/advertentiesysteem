package be.zoekerscript.gebruikers.berichten;

/**
 * Created by Caroline on 24-9-2016.
 */
public enum BerichtStatus {

    GELEZEN,
    VERZONDEN;

    private BerichtStatus(){
    }

}

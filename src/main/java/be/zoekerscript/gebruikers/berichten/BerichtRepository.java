package be.zoekerscript.gebruikers.berichten;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Created by Caroline on 29-9-2016.
 */
@Transactional
public interface BerichtRepository extends JpaRepository<Bericht, Integer> {

    @Query("SELECT b FROM Bericht b WHERE b.gebruikerVan = ?1")
    public default Page<Bericht> findByVerzender(Integer gebruikersId, Pageable pageable) {
        return this.findAll(pageable);
    }

    @Query("SELECT b FROM Bericht b WHERE b.gebruikerNaar = ?1")
    public default Page<Bericht> findByOntvanger(Integer gebruikersId, Pageable pageable) {
        return this.findAll(pageable);
    }


}

package be.zoekerscript.gebruikers.berichten;

/**
 * Created by Caroline on 29-9-2016.
 */
public class InvalidBerichtException extends Exception {
    public InvalidBerichtException(String message) {super(message);}
}

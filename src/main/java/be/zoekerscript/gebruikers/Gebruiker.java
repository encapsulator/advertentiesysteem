package be.zoekerscript.gebruikers;

import be.zoekerscript.gebruikers.recentie.Recentie;
import be.zoekerscript.gebruikers.recentie.RecentieException;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.function.IntPredicate;
import java.util.stream.Collectors;

/**
 * Created by Caroline on 18-9-2016.
 */
@Entity
@Table

public class Gebruiker implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int id;
    @Column
    private String gebruikersnaam;
    @Column
    private String mail;
    @Column
    private String wachtwoord;
    @Column
    private Date datum_lid;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "aan")
    private Collection<Recentie> recentiesOntvangen;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "van")
    private Collection<Recentie> recentiesGegeven;

    @Transient
    private Gebruikersrechten gebruikersrechten;
    @Column
    private String rechten;
    @Column
    private boolean accountGeactiveerd;
    private static final IntPredicate POSITIEVE_RECENTIES = new IntPredicate() {
        @Override
        public boolean test(int value) {
            return value > 0;
        }
    };

    public Gebruiker() {
    }

    public Gebruiker(String gebruikersnaam, String mail, String wachtwoord, Date datum_lid, Collection<Recentie> recentiesOntvangen, Gebruikersrechten gebruikersrechten) {
        this.gebruikersnaam = gebruikersnaam;
        this.mail = mail;
        this.wachtwoord = wachtwoord;
        this.datum_lid = datum_lid;
        this.recentiesOntvangen = recentiesOntvangen;
        this.gebruikersrechten = gebruikersrechten;
        this.rechten = gebruikersrechten.name();
    }

    public Gebruiker(int id, String gebruikersnaam, String mail, String wachtwoord, Date datum_lid, Collection<Recentie> recentiesOntvangen, Gebruikersrechten gebruikersrechten) {
        this(gebruikersnaam, mail, wachtwoord, datum_lid, recentiesOntvangen, gebruikersrechten);
        this.id = id;
    }

    public Gebruiker(int id, String gebruikersnaam, String mail, String wachtwoord, Date datum_lid, Collection<Recentie> recentiesOntvangen, Gebruikersrechten gebruikersrechten, boolean accountGeactiveerd) {
        this(id, gebruikersnaam, mail, wachtwoord, datum_lid, recentiesOntvangen, gebruikersrechten);
        this.accountGeactiveerd = accountGeactiveerd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getGebruikersnaam() {
        return gebruikersnaam;
    }

    public void setGebruikersnaam(String gebruikersnaam) {
        this.gebruikersnaam = gebruikersnaam;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getWachtwoord() {
        return wachtwoord;
    }

    public void setWachtwoord(String wachtwoord) {
        this.wachtwoord = wachtwoord;
    }

    public Date getDatum_lid() {
        return datum_lid;
    }

    public void setDatum_lid(Date datum_lid) {
        this.datum_lid = datum_lid;
    }

    public Collection<Recentie> getRecentiesOntvangen() {
        return recentiesOntvangen;
    }

    public void setRecentiesOntvangen(Collection<Recentie> recenties) {
        this.recentiesOntvangen = recenties;
    }

    public void addRecentie(Recentie recentie) throws RecentieException {
        if(id == recentie.getVan().id) throw new RecentieException("SAME_USER");
        recentiesOntvangen.add(recentie);
    }

    public int getTotaleRecentieScore() {
        if(recentiesOntvangen.size()==0) return 0;
        return recentiesOntvangen.stream().mapToInt(Recentie::getRating).sum();
    }

    public Collection<Recentie> getPositieveRecenties() {
        if(recentiesOntvangen.size()==0) return recentiesOntvangen;
        return recentiesOntvangen.stream().filter(recentie -> recentie.getRating()==1).collect(Collectors.toList());
    }

    public Collection<Recentie> getNeutraleRecenties() {
        if(recentiesOntvangen.size()==0) return recentiesOntvangen;
        return recentiesOntvangen.stream().filter(recentie -> recentie.getRating()==0).collect(Collectors.toList());
    }

    public Collection<Recentie> getNegatieveRecenties() {
        if(recentiesOntvangen.size()==0) return recentiesOntvangen;
        return recentiesOntvangen.stream().filter(recentie -> recentie.getRating()==-1).collect(Collectors.toList());
    }

    public Gebruikersrechten getGebruikersrechten() {
        return gebruikersrechten;
    }

    public void setGebruikersrechten(Gebruikersrechten gebruikersrechten) {
        this.gebruikersrechten = gebruikersrechten;
    }

    public boolean isAccountGeactiveerd() {
        return accountGeactiveerd;
    }

    public void setAccountGeactiveerd(boolean accountGeactiveerd) {
        this.accountGeactiveerd = accountGeactiveerd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Gebruiker gebruiker = (Gebruiker) o;

        if (id != gebruiker.id) return false;
        return gebruikersnaam.equals(gebruiker.gebruikersnaam);

    }

}

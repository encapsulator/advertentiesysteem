package be.zoekerscript.gebruikers;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Caroline on 29-9-2016.
 */
public interface GebruikerRepository extends JpaRepository<Gebruiker, Integer> {
}

package be.zoekerscript.gebruikers;

/**
 * Created by Caroline on 18-9-2016.
 */
public enum Gebruikersrechten {

    GAST("Gast"),
    LID("Lid"),
    ADMINISTRATOR("Administrator"),
    ADMIN("Admin");

    private String type;

    private Gebruikersrechten(String gebruikersType) {
        type = gebruikersType;
    }

}

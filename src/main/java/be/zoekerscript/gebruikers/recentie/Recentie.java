package be.zoekerscript.gebruikers.recentie;

import be.zoekerscript.gebruikers.Gebruiker;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.*;
import java.util.Date;

/**
 * Created by Caroline on 18-9-2016.
 */
@Entity
@Table
public class Recentie {

    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne(cascade = CascadeType.DETACH , fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private Gebruiker van;

    @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER)
    @JoinColumn(referencedColumnName = "id", nullable = false)
    private Gebruiker aan;

    @Column
    private int rating;
    @Column
    private String boodschap;
    @Column
    private Date datum;

    public Recentie() {
    }

    public Recentie(Gebruiker van, Gebruiker aan, int rating, String boodschap, Date datum) throws RecentieException {
        this.van = van;
        this.aan = aan;
        if(rating < -1 || rating > 1) throw new RecentieException("RATING_MUST_BE_BETWEEN_-1_AND_1");
        this.rating = rating;
        this.boodschap = boodschap;
        this.datum = datum;
    }

    public Recentie(Integer id, Gebruiker van, Gebruiker aan, int rating, String boodschap, Date datum) throws RecentieException {
        this(van, aan, rating, boodschap, datum);
        this.id = id;
    }

    public Gebruiker getVan() {
        return van;
    }

    public void setVan(Gebruiker van) {
        this.van = van;
    }

    public Gebruiker getAan() {
        return aan;
    }

    public void setAan(Gebruiker aan) {
        this.aan = aan;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getBoodschap() {
        return boodschap;
    }

    public void setBoodschap(String boodschap) {
        this.boodschap = boodschap;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }
}

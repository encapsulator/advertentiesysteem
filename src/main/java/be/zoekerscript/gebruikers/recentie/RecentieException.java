package be.zoekerscript.gebruikers.recentie;

/**
 * Created by Caroline on 28-9-2016.
 */
public class RecentieException extends Exception {

    public RecentieException(String message) {
        super(message);
    }

}

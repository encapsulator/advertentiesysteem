package be.zoekerscript.gebruikers.recentie;

import org.assertj.core.api.IntegerAssert;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Caroline on 29-9-2016.
 */
public interface RecentieRepository extends JpaRepository<Recentie, Integer> {

}

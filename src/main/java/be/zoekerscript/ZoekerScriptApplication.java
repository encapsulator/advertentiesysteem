package be.zoekerscript;

import be.zoekerscript.gebruikers.Gebruiker;
import be.zoekerscript.gebruikers.GebruikerRepository;
import be.zoekerscript.gebruikers.Gebruikersrechten;
import be.zoekerscript.gebruikers.berichten.Bericht;
import be.zoekerscript.gebruikers.berichten.BerichtRepository;
import be.zoekerscript.gebruikers.recentie.Recentie;
import be.zoekerscript.zoekertjes.DefaultState;
import be.zoekerscript.zoekertjes.ZoekerRepository;
import be.zoekerscript.zoekertjes.ZoekerSettings;
import be.zoekerscript.zoekertjes.Zoekertje;
import be.zoekerscript.zoekertjes.categorie.Categorie;
import be.zoekerscript.zoekertjes.reactie.Reactie;
import be.zoekerscript.zoekertjes.veiling.Bod;
import be.zoekerscript.zoekertjes.veiling.Veiling;
import com.sun.glass.ui.Application;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;

import java.util.ArrayList;
import java.util.Date;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
public class ZoekerScriptApplication {

	private static final Logger log = LoggerFactory.getLogger(Application.class);
	private static final boolean RUNDBTEST = false;

	public static void main(String[] args) {
		SpringApplication.run(ZoekerScriptApplication.class);
	}

	@Bean
	public CommandLineRunner demo(GebruikerRepository repository, BerichtRepository berichtRepository, ZoekerRepository zoekerRepository) {
		if(!RUNDBTEST) return (args)->{};
		return (args) -> {
			Gebruiker niels = new Gebruiker("niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
			repository.save(niels);
			Gebruiker simon = new Gebruiker("simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
			repository.save(simon);
			Gebruiker mark = new Gebruiker("mark", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
			repository.save(mark);
			Gebruiker an = new Gebruiker("an", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
			repository.save(an);
			Gebruiker els = new Gebruiker("els", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
			repository.save(els);

			niels.addRecentie(new Recentie(simon, niels, -1, "slecht", new Date()));
			niels.addRecentie(new Recentie(mark, niels, 0, "meh", new Date()));
			niels.addRecentie(new Recentie(an, niels, 1, "goed", new Date()));
			niels.addRecentie(new Recentie(els, niels, 1, "goed", new Date()));

			repository.save(niels);

			//berichten

			ArrayList<Bericht> berichtenBank = new ArrayList<>();



			Bericht origineelBericht = new Bericht(simon.getId(), niels.getId(), "En voor welke prijs?", "Is de website te koop?", "VERZONDEN", null, new Date());
			origineelBericht = berichtRepository.save(origineelBericht);
			berichtenBank.add(origineelBericht);

			Supplier<Stream<Bericht>> berichtenInboxNiels = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == niels.getId());
			Bericht berichtOntvangen = berichtenInboxNiels.get().collect(Collectors.toList()).get((int) berichtenInboxNiels.get().count() - 1);
			berichtOntvangen.setBerichtStatus("GELEZEN");

			Bericht reactieNiels = new Bericht(niels.getId(), simon.getId(), "Ja ze is zeker nog te koop.", "BLABLA", "VERZONDEN", berichtOntvangen, new Date());
			reactieNiels = berichtRepository.save(reactieNiels);
			berichtenBank.add(reactieNiels);

			Supplier<Stream<Bericht>> berichtenInboxSimon = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == simon.getId());

			Bericht tweedeReactieNiels = new Bericht(niels.getId(), simon.getId(), "Vaste prijs is €1500.", "BLABLA", "VERZONDEN", berichtOntvangen, new Date());
			tweedeReactieNiels = berichtRepository.save(tweedeReactieNiels);
			berichtenBank.add(tweedeReactieNiels);

			berichtenInboxSimon = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == simon.getId());
			Bericht tweedeBerichtOntvangenSimon = berichtenInboxSimon.get().collect(Collectors.toList()).get((int) berichtenInboxSimon.get().count() - 1);
			tweedeBerichtOntvangenSimon.setBerichtStatus("GELEZEN");

			Bericht reactieSimon = new Bericht(simon.getId(), niels.getId(), "Oké dat is genoteerd.", "BLABLA", "VERZONDEN", tweedeBerichtOntvangenSimon, new Date());
			berichtRepository.save(reactieSimon);

			//Zoekertje met veiling en boden en andere benodigdheden
			Categorie categorie = new Categorie("Scripts");
			Veiling veiling = new Veiling(250, 750, 25);

			Zoekertje zoekertje = new Zoekertje.ZoekertjesBuilder().setTitel("Veilingscript te koop").setTekst("PHP script met bijhorende website")
					.setDatumToegevoegd(new Date()).setCategorie(categorie).setGebruikersId(niels.getId()).setVeiling(veiling)
					.setAantal_bekeken(5).setZoekerTypeState(new DefaultState()).build();


			zoekertje.addBod(new Bod(simon.getId(), 350, new Date(), veiling));
			zoekertje.addBod(new Bod(els.getId(), 750, new Date(), veiling));

			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), simon.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), an.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), els.getId(), zoekertje));
			zoekertje.addReactie(new Reactie("Mijn laatste reactie", new Date(), niels.getId(), zoekertje));

			zoekerRepository.save(zoekertje);


		};
	}

}

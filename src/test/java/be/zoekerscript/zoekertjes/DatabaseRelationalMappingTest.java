package be.zoekerscript.zoekertjes;

import be.zoekerscript.gebruikers.Gebruiker;
import be.zoekerscript.gebruikers.GebruikerRepository;
import be.zoekerscript.gebruikers.Gebruikersrechten;
import be.zoekerscript.gebruikers.berichten.Bericht;
import be.zoekerscript.gebruikers.berichten.BerichtRepository;
import be.zoekerscript.gebruikers.berichten.InvalidBerichtException;
import be.zoekerscript.gebruikers.recentie.Recentie;
import be.zoekerscript.gebruikers.recentie.RecentieException;
import be.zoekerscript.gebruikers.recentie.RecentieRepository;
import be.zoekerscript.zoekertjes.categorie.Categorie;
import be.zoekerscript.zoekertjes.categorie.CategorieRepository;
import be.zoekerscript.zoekertjes.reactie.Reactie;
import be.zoekerscript.zoekertjes.reactie.ReactieException;
import be.zoekerscript.zoekertjes.reactie.ReactieRepository;
import be.zoekerscript.zoekertjes.veiling.*;
import org.hsqldb.util.DatabaseManager;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by Niels on 10/1/2016.
 */
@SpringBootTest
@RunWith(value = SpringRunner.class)
@ActiveProfiles("test")
public class DatabaseRelationalMappingTest {

    @Autowired
    private GebruikerRepository gebruikerRepository;
    @Autowired
    private ZoekerRepository zoekerRepository;
    @Autowired
    private BodRepository bodRepository;
    @Autowired
    private ReactieRepository reactieRepository;
    @Autowired
    private CategorieRepository categorieRepository;
    @Autowired
    private ZoekerSettingsRepository zoekerSettingsRepository;
    @Autowired
    private VeilingRepository veilingRepository;
    @Autowired
    private BerichtRepository berichtRepository;
    @Autowired
    private RecentieRepository recentieRepository;
    @Before
    public void setUp() {
        System.setProperty("java.awt.headless", "false");
        org.hsqldb.util.DatabaseManagerSwing.main(new String[] {
                "--url",  "jdbc:hsqldb:mem:testdb", "--noexit"
        });
    }

    @Test
    public void kanZoekertjeEnDependenciesAanmakenOphalenWijzigenVerwijderen () throws BodInvalidException, ReactieException {
        Gebruiker niels = new Gebruiker("niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        niels = gebruikerRepository.save(niels);
        Gebruiker simon = new Gebruiker("simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        simon = gebruikerRepository.save(simon);
        Gebruiker mark = new Gebruiker("mark", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        mark = gebruikerRepository.save(mark);
        Gebruiker an = new Gebruiker("an", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        an = gebruikerRepository.save(an);
        Gebruiker els = new Gebruiker("els", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        els = gebruikerRepository.save(els);

        Categorie categorie = new Categorie("Scripts");
        Veiling veiling = new Veiling(250, 750, 25);

        Zoekertje zoekertje = new Zoekertje.ZoekertjesBuilder().setTitel("Veilingscript te koop").setTekst("PHP script met bijhorende website")
                .setDatumToegevoegd(new Date()).setCategorie(categorie).setGebruikersId(niels.id).setVeiling(veiling)
                .setAantal_bekeken(5).setZoekerTypeState(new DefaultState()).build();


        zoekertje.addBod(new Bod(simon.id, 350, new Date(), veiling));
        zoekertje.addBod(new Bod(els.id, 650, new Date(), veiling));

        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), simon.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), an.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), els.id, zoekertje));
        zoekertje.addReactie(new Reactie("Mijn laatste reactie", new Date(), niels.id, zoekertje));

        zoekertje = zoekerRepository.save(zoekertje);

        zoekertje = zoekerRepository.findOne(zoekertje.getId());

        //Create
        assertThat(zoekertje.getTitel()).isEqualTo("Veilingscript te koop");
        assertThat(zoekertje.getCategorie().getNaam()).isEqualTo("Scripts");
        assertThat(zoekertje.getVeiling().getBoden().size()).isEqualTo(2);
        assertThat(zoekertje.getReacties().size()).isEqualTo(8);

        assertThat(zoekertje.getVeiling().getBoden().stream().collect(Collectors.toList()).get(0).getWaarde()).isEqualTo(350);

        //Update
        zoekertje.getVeiling().getBoden().clear();
        zoekertje = zoekerRepository.save(zoekertje);
        assertThat(zoekertje.getVeiling().getBoden().size()).isEqualTo(0);
        Collection<Bod> bodenInDatabank = bodRepository.findAll();
        assertThat(bodenInDatabank.size()).isEqualTo(0);

        zoekertje.getReacties().stream().collect(Collectors.toList()).get(zoekertje.getReacties().size() - 1).setBoodschap("hoi");
        zoekertje = zoekerRepository.save(zoekertje);

        assertThat(zoekertje.getReacties().stream().collect(Collectors.toList()).get(zoekertje.getReacties().size() - 1)
                .getBoodschap()).isEqualTo("hoi");

        //Delete
        zoekerRepository.delete(zoekertje);
        assertThat(reactieRepository.findAll().size()).isEqualTo(0);
        assertThat(zoekerSettingsRepository.findAll().size()).isEqualTo(0);
        assertThat(veilingRepository.findAll().size()).isEqualTo(0);
        assertThat(categorieRepository.findAll().size()).isEqualTo(1);
    }

    @Test
    public void kanGebruikerEnDependenciesAanmakenOphalenWijzigenVerwijderen () throws RecentieException, InvalidBerichtException {
        //Create without dependencies
        Gebruiker niels = new Gebruiker("niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        gebruikerRepository.save(niels);
        Gebruiker simon = new Gebruiker("simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        gebruikerRepository.save(simon);
        Gebruiker mark = new Gebruiker("mark", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        gebruikerRepository.save(mark);
        Gebruiker an = new Gebruiker("an", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        gebruikerRepository.save(an);
        Gebruiker els = new Gebruiker("els", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        gebruikerRepository.save(els);

        //Create dependency recentie
        niels.addRecentie(new Recentie(simon, niels, -1, "slecht", new Date()));
        niels.addRecentie(new Recentie(mark, niels, 0, "meh", new Date()));
        niels.addRecentie(new Recentie(an, niels, 1, "goed", new Date()));
        niels.addRecentie(new Recentie(els, niels, 1, "goed", new Date()));
        niels = gebruikerRepository.save(niels);

        assertThat(gebruikerRepository.count()).isEqualTo(5);
        assertThat(recentieRepository.count()).isEqualTo(4);

        assertThat(recentieRepository.findOne(4).getVan().getGebruikersnaam()).isEqualTo("els");

        //Create & retrieve dependency berichten
        Bericht origineelBericht = new Bericht(simon.id, niels.id, "En voor welke prijs?", "Is de website te koop?", "VERZONDEN", null, new Date());
        origineelBericht = berichtRepository.save(origineelBericht);

        origineelBericht.setBerichtStatus("GELEZEN");
        berichtRepository.save(origineelBericht);
        Bericht berichtOntvangen = berichtRepository.findByOntvanger(niels.id, new PageRequest(0, 1)).getContent().get(0);
        String a = "b";
        Bericht reactieNiels = new Bericht(niels.id, simon.id, "Ja ze is zeker nog te koop.", "BLABLA", "VERZONDEN", berichtOntvangen, new Date());
        berichtRepository.save(reactieNiels);

        Bericht tweedeReactieNiels = new Bericht(niels.id, simon.id, "Vaste prijs is €1500.", "BLABLA", "GELEZEN", berichtOntvangen, new Date());
        berichtRepository.save(tweedeReactieNiels);

        Bericht tweedeBerichtOntvangenSimon = berichtRepository.findByOntvanger(simon.id, new PageRequest(1, 1)).getContent().get(0);
        tweedeBerichtOntvangenSimon.setBerichtStatus("GELEZEN");
        berichtRepository.save(tweedeBerichtOntvangenSimon);

        Bericht reactieSimon = new Bericht(simon.id, niels.id, "Oké dat is genoteerd.", "BLABLA", "VERZONDEN", tweedeBerichtOntvangenSimon, new Date());
        berichtRepository.save(reactieSimon);

        assertThat(berichtRepository.count()).isEqualTo(4);
        assertThat(berichtRepository.findOne(4).getParent().getParent().getBericht()).isEqualTo("En voor welke prijs?");
        assertThat(berichtRepository.findOne(2).getBerichtStatus()).isEqualTo("GELEZEN");
        assertThat(berichtRepository.findOne(1).getParent()).isNull();
        assertThat(berichtRepository.findOne(4).getParent().getParent().getGebruikerVan()).isEqualTo(simon.id);



    }

}

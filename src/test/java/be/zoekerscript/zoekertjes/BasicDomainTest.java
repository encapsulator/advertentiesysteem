package be.zoekerscript.zoekertjes;

import be.zoekerscript.gebruikers.berichten.Bericht;
import be.zoekerscript.gebruikers.berichten.InvalidBerichtException;
import be.zoekerscript.gebruikers.Gebruiker;
import be.zoekerscript.gebruikers.Gebruikersrechten;
import be.zoekerscript.gebruikers.recentie.Recentie;
import be.zoekerscript.gebruikers.recentie.RecentieException;
import be.zoekerscript.zoekertjes.reactie.Reactie;
import be.zoekerscript.zoekertjes.reactie.ReactieException;
import be.zoekerscript.zoekertjes.categorie.Categorie;
import be.zoekerscript.zoekertjes.veiling.Bod;
import be.zoekerscript.zoekertjes.veiling.BodInvalidException;
import be.zoekerscript.zoekertjes.veiling.Veiling;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;


/**
 * Created by Caroline on 24-9-2016.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class BasicDomainTest {

    private Zoekertje zoekertje;

    @Test
    public void kanZoekertjeMakenEnCorrectOpbieden() throws BodInvalidException {

        Gebruiker gebruiker = new Gebruiker(0, "encapsulator", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker gebruiker1 = new Gebruiker(1, "simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Categorie categorie = new Categorie("Scripts");
        Veiling veiling = new Veiling(250, 750, 25);

        Zoekertje zoekertje = new Zoekertje.ZoekertjesBuilder().setTitel("Veilingscript te koop").setTekst("PHP script met bijhorende website")
                .setDatumToegevoegd(new Date()).setCategorie(categorie).setGebruikersId(gebruiker.getId()).setVeiling(veiling)
                .setAantal_bekeken(5).setZoekerTypeState(new DefaultState()).build();

        assertThat(zoekertje.getCategorie().getNaam()).isEqualTo("Scripts");

        assertThatThrownBy(() -> { zoekertje.addBod(new Bod(0, 249, new Date())); }).hasMessage("USER_OWNS_ZOEKERTJE");
        assertThatThrownBy(() -> { zoekertje.addBod(new Bod(1, 249, new Date())); }).hasMessage("BOD_UNDER_MINIMUM");
        Bod bod = new Bod(1, 350, new Date());

        zoekertje.addBod(bod);

        assertThatThrownBy(() -> { zoekertje.addBod(new Bod(1, 250, new Date())); }).hasMessage("ALREADY_HIGHER_BOD");
        zoekertje.addBod(new Bod(1, 750, new Date()));
        assertThatThrownBy(() -> { zoekertje.addBod(new Bod(1, 751, new Date())); }).hasMessage("BOD_OVER_BUY_NOW");

        assertThat(gebruiker1.getId()).isEqualTo(zoekertje.getVeiling().getHoogsteBod().getGebruikersId());

    }

    @Test
    public void testKanZoekerStatusCorrectWijzigen () throws ZoekerStateException {

        Gebruiker gebruiker = new Gebruiker(0, "encapsulator", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Categorie categorie = new Categorie("Scripts");
        Veiling veiling = new Veiling(250, 750, 25);

        Zoekertje zoekertje = new Zoekertje.ZoekertjesBuilder().setTitel("Veilingscript te koop").setTekst("PHP script met bijhorende website")
                .setDatumToegevoegd(new Date()).setCategorie(categorie).setGebruikersId(gebruiker.getId()).setVeiling(veiling)
                .setAantal_bekeken(5).setZoekerTypeState(new DefaultState()).build();

        zoekertje.changeZoekerTypeState(new PromoState());
        zoekertje.changeZoekerTypeState(new SpotlightState());
        zoekertje.changeZoekerTypeState(new DefaultState());
        zoekertje.changeZoekerTypeState(new SpotlightState());

        assertThatThrownBy(() -> { zoekertje.changeZoekerTypeState(new PromoState()); }).hasMessage("ALREADY_PROMO");
        assertThatThrownBy(() -> { zoekertje.changeZoekerTypeState(new SpotlightState()); }).hasMessage("SAME_STATE");

    }

    @Test
    public void testKanRecentiesToekennenEnOpvragen () throws RecentieException {

        Gebruiker niels = new Gebruiker(0, "niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker simon = new Gebruiker(1, "simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker mark = new Gebruiker(2, "mark", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker an = new Gebruiker(3, "an", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker els = new Gebruiker(4, "els", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);

        assertThatThrownBy(() -> { niels.addRecentie(new Recentie(niels, niels, -1, "slecht", new Date())); }).hasMessage("SAME_USER");


        niels.addRecentie(new Recentie(simon, niels, -1, "slecht", new Date()));
        niels.addRecentie(new Recentie(mark, niels, 0, "meh", new Date()));
        niels.addRecentie(new Recentie(an, niels, 1, "goed", new Date()));
        niels.addRecentie(new Recentie(els, niels, 1, "goed", new Date()));

        assertThatThrownBy(() -> { niels.addRecentie(new Recentie(niels, niels, -5, "slecht", new Date())); }).hasMessage("RATING_MUST_BE_BETWEEN_-1_AND_1");

        assertThat(niels.getPositieveRecenties().stream().mapToInt(Recentie::getRating).count()).isEqualTo(2);
        assertThat(niels.getNegatieveRecenties().stream().mapToInt(Recentie::getRating).count()).isEqualTo(1);
        assertThat(niels.getNeutraleRecenties().stream().mapToInt(Recentie::getRating).count()).isEqualTo(1);

        assertThat(niels.getTotaleRecentieScore()).isEqualTo(1);

    }

    @Test
    public void testKanReactiesPlaatsenEnOphalenVanZoekertje() throws ReactieException {
        Gebruiker niels = new Gebruiker(0, "niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker simon = new Gebruiker(1, "simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker mark = new Gebruiker(2, "mark", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker an = new Gebruiker(3, "an", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker els = new Gebruiker(4, "els", "mark@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);

        Categorie categorie = new Categorie("Scripts");
        Veiling veiling = new Veiling(250, 750, 25);

        Zoekertje zoekertje = new Zoekertje.ZoekertjesBuilder().setTitel("Veilingscript te koop").setTekst("PHP script met bijhorende website")
                .setDatumToegevoegd(new Date()).setCategorie(categorie).setGebruikersId(niels.getId()).setVeiling(veiling)
                .setAantal_bekeken(5).setZoekerTypeState(new DefaultState()).build();

        assertThatThrownBy(() -> { zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.getId())); }).hasMessage("OWNER_CANT_PLACE_FIRST_REACTION");

        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), simon.getId()));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId()));

        assertThatThrownBy(() -> {zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId())); }).hasMessage("SAME_USER_LAST_REACTION");

        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.getId()));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId()));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), an.getId()));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), niels.getId()));
        zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), els.getId()));
        zoekertje.addReactie(new Reactie("Mijn laatste reactie", new Date(), niels.getId()));

        Collection<Reactie> reacties = zoekertje.getReacties();
        assertThat(reacties.stream().collect(Collectors.toList()).get(reacties.size()-1).getBoodschap()).isEqualTo("Mijn laatste reactie");

        zoekertje.setReacties(new ArrayList<>());
        ZoekerSettings settings = new ZoekerSettings(null, null, false, true);
        zoekertje.setSettings(settings);

        assertThatThrownBy(() -> { zoekertje.addReactie(new Reactie("Mijn reactie", new Date(), mark.getId())); }).hasMessage("NOT_ALLOWED");

    }

    @Test
    public void kanBerichtenVersturenEnAntwoordenOpBerichtenTussenGebruikers() throws InvalidBerichtException {

        ArrayList<Bericht> berichtenBank = new ArrayList<>();

        Gebruiker niels = new Gebruiker(0, "niels", "nielshvermeiren@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);
        Gebruiker simon = new Gebruiker(1, "simon", "simon@gmail.com", "NiEWk785", new Date(), new ArrayList<Recentie>(), Gebruikersrechten.LID);

        Bericht origineelBericht = new Bericht(1, 0, "En voor welke prijs?", "Is de website te koop?", "VERZONDEN", null, new Date());
        berichtenBank.add(origineelBericht);

        Supplier<Stream<Bericht>> berichtenInboxNiels = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == 0);
        Bericht berichtOntvangen = berichtenInboxNiels.get().collect(Collectors.toList()).get((int) berichtenInboxNiels.get().count() - 1);
        berichtOntvangen.setBerichtStatus("GELEZEN");

        assertThat(berichtOntvangen.getBericht()).isEqualTo("En voor welke prijs?");

        Bericht reactieNiels = new Bericht(0, 1, "Ja ze is zeker nog te koop.", "BLABLA", "VERZONDEN", berichtOntvangen, new Date());
        berichtenBank.add(reactieNiels);

        Supplier<Stream<Bericht>> berichtenInboxSimon = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == 1);
        Bericht berichtOntvangenSimon = berichtenInboxSimon.get().collect(Collectors.toList()).get((int) berichtenInboxSimon.get().count() - 1);
        berichtOntvangen.setBerichtStatus("GELEZEN");

        assertThat(berichtOntvangenSimon.getBericht()).isEqualTo("Ja ze is zeker nog te koop.");
        assertThat(berichtOntvangenSimon.getTitel()).isEqualTo("RE: Is de website te koop?");

        Bericht tweedeReactieNiels = new Bericht(0, 1, "Vaste prijs is €1500.", "BLABLA", "VERZONDEN", berichtOntvangen, new Date());
        berichtenBank.add(tweedeReactieNiels);

        berichtenInboxSimon = () -> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == 1);
        Bericht tweedeBerichtOntvangenSimon = berichtenInboxSimon.get().collect(Collectors.toList()).get((int) berichtenInboxSimon.get().count() - 1);
        tweedeBerichtOntvangenSimon.setBerichtStatus("GELEZEN");

        assertThat(tweedeBerichtOntvangenSimon.getBericht()).isEqualTo("Vaste prijs is €1500.");
        assertThat(tweedeBerichtOntvangenSimon.getTitel()).isEqualTo("RE: Is de website te koop?");

        Bericht reactieSimon = new Bericht(1, 0, "Oké dat is genoteerd.", "BLABLA", "VERZONDEN", tweedeBerichtOntvangenSimon, new Date());
        berichtenBank.add(reactieSimon);

        berichtenInboxNiels = ()-> berichtenBank.stream().filter(b -> b.getGebruikerNaar() == 0);
        Bericht tweedeBerichtOntvangenNiels = berichtenInboxNiels.get().collect(Collectors.toList()).get((int) berichtenInboxNiels.get().count() - 1);
        tweedeBerichtOntvangenNiels.setBerichtStatus("GELEZEN");

        assertThat(tweedeBerichtOntvangenNiels.getBericht()).isEqualTo("Oké dat is genoteerd.");
        assertThat(tweedeBerichtOntvangenNiels.getTitel()).isEqualTo("RE: RE: Is de website te koop?");

        assertThat(tweedeBerichtOntvangenNiels.getParent().getParent().getBericht()).isEqualTo("En voor welke prijs?");
        assertThat(tweedeBerichtOntvangenNiels.getParent().getParent().getTitel()).isEqualTo("Is de website te koop?");

        assertThatThrownBy(() -> { new Bericht(1, 1, "haha.", "Halo aan mezelf", "VERZONDEN", null, new Date()); }).hasMessage("SAME_USER");

    }

}